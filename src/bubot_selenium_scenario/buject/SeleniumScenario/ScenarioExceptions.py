class Capcha(Exception):
    pass


class Suspicious(Exception):
    pass


class NotLoaded(Exception):
    pass
